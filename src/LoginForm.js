import React, { Component } from "react";
import axios from "axios";

export default class LoginForm extends Component{
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            isValid: true
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleLogIn = this.handleLogIn.bind(this);
    }

    handleInputChange(event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleLogIn(){
        let res = axios.post("<some login api>", {login: this.state.login, password: this.state.password});
        this.setState({isValid: res});
    }

    render(){
        let value = (
            <div>
                <div>
                    Login:
                    <input type="text" name="login" onChange={this.handleInputChange}/>
                </div>
                <div>
                    Password:
                    <input type="text" name="password" onChange={this.handleInputChange}/>
                </div>
                <button onClick={this.handleLogIn}>
                    Log in
                </button>
            </div>
        );

        return value;
    }
}